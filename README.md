# Drupal multi project & multi site based on drupal4docker project #

## Requirements ##
What you need to proceed:

* docker & docker-compose available
* mkcert - https://github.com/FiloSottile/mkcert

## Contents ##
Files:
* get-certkey.sh - bash script that generates cert.pem and key.pem in certs/ folder. Those files are used for https.
* start.sh - bash script that will start drupal7, drupal8 and traefik containers.
* stop.sh - bash script that will stop all containers start script initiated.
* destroy.sh - bash script to destroy (down) all containers.
* default.bashrc - this file will be mounted as ~/.bashrc file on php containers. It adds some aliases and PS1 format.

## Add new project ##
Easiest way would be to simply copy existing (drupal7 or drupal8 folder) and update .env file as well as docker-compose.yml file. You will need to update scripts (start/stop/destroy) with this new project.

Beware this build is drupal based build, there is some configuration that will prevent other PHP applications to run, for example you may not be allowed to open thisisme.php file from your browser.

## Add new site in multisite setup ###
Update sites.php file located in sites/ folder, and create folder with configured name. You can copy settings.php from default/ folder to newly created site folder. 

If using multi site on one database, make sure you use prefix. OR you can duplicate mariadb service and rename it.

### Links ###

* portainer - http://localhost:9000
* traefik - http://localhost:8080
* drupal7 - https://drupal7.drupal.docker.localhost/ (http://drupal7.drupal.docker.localhost:8000/)
* drupal8 - https://drupal8.drupal.docker.localhost/ (http://drupal8.drupal.docker.localhost:8000/)

Currently both drupal7/drupal8 have only phpinfo file as index.php.

### How to use it ###
To start all containers run start.sh script.

To stop running containers run stop.sh.

To destroy all created containers run destroy.sh

## Times ###
* Windows box is kind a old one (i7 3770 with 32 GB ram)
* Linux1 is T470 (i7 7500 with 32 GB ram)
* Linux2 is desktop (i7 4770 with 32 GB ram)
* Mac (i7 7820HQ 16 GB ram)

OS\Action| D7 Drush dl | D7 Drush si | D8 Composer | D8 Drush si | D7 WCMS Drush si 
:---:|:---:|:---:|:---:|:---:|:---:
Win10|18s|1:55s 2:33s 2:13s|9:27s|1:57s 3:27s 3:07s|
Linux 1|2s|10s 10s 10s|3:04s|17s 17s 17s| 10:20s
Linux 2|2s|6s 6s 6s|3:03s|13s 13s 13s|6:35s 6:09s 6:35s
Mac | 6s | 18s 18s 18s | 5:30s | 1:04s 1:01s 1:03s|
