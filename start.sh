#!/usr/bin/env bash
TIMEPREFIX=''

if [ $1 == '-t' ] && [ -f /usr/bin/time ]; then
  echo "Runnig timed commands."
  TIMEPREFIX='/usr/bin/time -f "total:%E"'
fi

echo "Check certs"
if [ ! -d certs ] || [ ! -f certs/cert.pem ] || [ ! -f certs/key.pem ]; then
  echo "Generating cert and key."
  $TIMEPREFIX ./gen-certkey.sh
else
  echo "Found cert.pem and key.pem"
fi

echo "Start D7"
cd drupal7
$TIMEPREFIX docker-compose up -d

echo "Start D8"
cd ../drupal8
$TIMEPREFIX docker-compose up -d

echo "Start Traefik"
cd ..
$TIMEPREFIX docker-compose up -d

echo "Done"