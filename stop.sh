#!/usr/bin/env bash
echo "Stopping traefik"
docker-compose stop

echo "Stoping D7"
cd drupal7
docker-compose stop

echo "Stoping D8"
cd ../drupal8
docker-compose stop

cd ..
echo "Done"
