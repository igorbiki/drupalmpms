#!/usr/bin/env bash
echo "Destroy traefik"
docker-compose down

echo "Destroy D7"
cd drupal7/
docker-compose down

echo "Destroy D8"
cd ../drupal8/
docker-compose down

echo "Done."