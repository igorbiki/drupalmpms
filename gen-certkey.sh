#!/usr/bin/env bash

echo "Check cert folder, if not exists create it."
[[ ! -d certs ]] && mkdir certs

CERTS="-cert-file certs/cert.pem -key-file certs/key.pem drupal7 drupal8 drupal7.drupal.docker.localhost drupal8.drupal.docker.localhost wcms7 wcms8"

echo "Generate cert and key file"
if [ -f mkcert.exe ]; then
  ./mkcert.exe $CERTS
else
  if type mkcert > /dev/null; then
    mkcert $CERTS
  elif [ -f mkcert ]; then
      ./mkcert $CERTS
  else
    echo "Command mkcert not found. See https://github.com/FiloSottile/mkcert for how to install."
  fi
fi

echo "Done"
